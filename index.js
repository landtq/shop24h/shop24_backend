
const express = require('express');
const mongoose = require('mongoose');

const app = new express();

const port = 8000;

app.use(express.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


const productTypeRouter = require("./app/routes/productTypeRouter");
const productRouter = require("./app/routes/productRouter");
const orderDetailRouter = require("./app/routes/orderDetailRouter");
const orderRouter = require("./app/routes/orderRouter");
const customerRouter = require("./app/routes/customerRouter")



mongoose.connect('mongodb://localhost:27017/CRUD_Shop24h', function (error) {
    if (error) throw error;
    console.log('connect success');
})


app.use('/', productTypeRouter);
app.use('/', productRouter);
app.use('/', orderDetailRouter);
app.use('/', orderRouter);
app.use('/', customerRouter);


app.listen(port, () => {
    console.log(`app listen on port ${port}`)
})


