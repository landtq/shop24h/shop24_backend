const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const customer = new Schema({
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: false
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        default: "",
    },
    city: {
        type: String,
        default: "",
    },
    country: {
        type: String,
        default: "",
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "orders"
    }]
}, {
    timestamps: true
});

module.exports = mongoose.model('customer', customer);