const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const orderDetail = new Schema({
    product: {
        type: mongoose.Types.ObjectId,
        ref: 'products'
    },
    quantity: {
        type: Number,
        default: 0
    }
},{
    timestamps: true
})

module.exports = mongoose.model('orderDetail',orderDetail);