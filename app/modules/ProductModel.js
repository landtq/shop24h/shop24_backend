const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const product = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
    },
    type: {
        type: mongoose.Types.ObjectId,
        ref: 'productType',
        required: true
    },
    imageUrl: {
        type: String,
        required: true,
    },
    buyPrice: {
        type: Number,
        required: true,
    },
    promotionPrice: {
        type: Number,
        required: false
    },
    amount: {
        type: Number,
        default: 0
    },
    color: [
        {
            type: String,
            require: true
        }
    ],
    categories: [
        {
            type: String,
            require: true
        }
    ],
    imageChild: [{
        type: String,
        required: true,
    }]
}, {
    timestamps: true
})

module.exports = mongoose.model('products', product); 