const mongoose = require('mongoose');

const productTypeModel = require("../modules/productTypeModel");

const createProductType = (req, res) => {
    let body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: `name is required`
        })
    }
    let newProductType = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
    }
    productTypeModel.create(newProductType, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newProductType: data
        })
    })
}

//get all productType
async function getAllProductType(req, res) {
    try {
        const limit = parseInt(req.query.limit) || 10;
        const page = parseInt(req.query.page) - 1 || 0;
        let filterName = req.query.filterName;
        const regex = new RegExp(`${filterName}`);
        let condition = {};
        if (filterName) {
            condition.name = regex;
        }
        const productType = await productTypeModel.find(condition)
            .skip(page * limit)
            .limit(limit)
        const total = await productTypeModel.countDocuments(condition)
        const response = {
            message: "get all product type successful",
            total,
            page: page + 1,
            limit,
            productType
        }
        res.status(200).json(response)
    } catch (error) {
        res.status(500).json(error)
    }
}


//get productType by Id
const getProductTypeById = (req, res) => {
    let productTypeId = req.params.productTypeId;
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: `productTypeId is invalid`
        })
    }
    productTypeModel.findById(productTypeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: ` get productType by Id success`,
            productType: data
        })
    })

}

//update productType by Id
const updateProductTypeById = (req, res) => {
    let productTypeId = req.params.productTypeId;
    let body = req.body;;
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: `productTypeId is invalid`
        })
    }
    if (body.name !== undefined && body.name == "") {
        return res.status(400).json({
            message: `name is required`
        })
    }
    let updateproductType = {
        name: body.name,
        description: body.description,
    }
    productTypeModel.findByIdAndUpdate(productTypeId, updateproductType, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updateCourse: data
        })
    })
}

//delete by productType Id
const deleteByProductTypeId = (req, res) => {
    let productTypeId = req.params.productTypeId;
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: `productTypeId is invalid`
        })
    }
    productTypeModel.findByIdAndDelete(productTypeId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}



module.exports = { createProductType, getAllProductType, getProductTypeById, updateProductTypeById, deleteByProductTypeId }