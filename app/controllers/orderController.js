const mongoose = require("mongoose");
const orderModel = require("../modules/OrderModel");
const customerModel = require("../modules/CustomerModel");

// create Order Of Customer
const createOrderOfCustomer = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let customerId = req.params.customerId;
    let body = req.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Customer ID is invalid"
        })
    }

    if (!body.shippedDate) {
        return res.status(400).json({
            message: "shipped Date is invalid"
        })
    }

    if (!Number.isInteger(body.cost) || body.cost < 0) {
        return res.status(400).json({
            message: "Cost is invalid!"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let newOrderInput = {
        _id: mongoose.Types.ObjectId(),
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost,
    }

    orderModel.create(newOrderInput, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            customerModel.findByIdAndUpdate(customerId,
                {
                    $push: { orders: data._id }
                },
                (err, updatedcustomer) => {
                    if (err) {
                        return ResizeObserver.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return res.status(201).json({
                            status: "Create Order Success",
                            data: data
                        })
                    }
                }
            )
        }
    })
}
// Get all Order 
const getAllOrder = (req, res) => {
    orderModel.find().populate({
        path: 'orderDetails',
        populate: {
            path: 'product',
            populate: {
                path: "type",
                model: "productType"
            }
        }
    }).exec((error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get all order success",
                data: data
            })
        }
    })
}

const getAllOrderOfCustomer = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let customerId = request.params.customerId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Customer ID is invalid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    customerModel.findById(customerId)
        // .populate("orders")
        .populate({
            path: 'orders',
            populate:
            {
                path: 'orderDetails',
                populate: {
                    path: 'product',
                    populate: {
                        path: "type",
                        model: "productType"
                    }
                }
            },
        })
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get data success",
                    data: data.orders
                })
            }
        })
}

const getOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "OrderId ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Get order success",
                data: data
            })
        }
    })
}

const updateOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = req.params.orderId;
    let body = req.body;

    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order ID is not valid"
        })
    }
    if (body.orderDate !== undefined && body.orderDate == "") {
        return res.status(400).json({
            message: "orderDate is invalid!"
        })
    }
    if (body.shippedDate !== undefined && body.shippedDate == "") {
        return res.status(400).json({
            message: "shippedDate is invalid!"
        })
    }
    if (body.cost !== undefined && (!Number.isInteger(body.cost) || body.cost <= 0)) {
        return res.status(400).json({
            message: "cost is invalid!"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let OrderUpdate = {
        orderDate: body.orderDate,
        shippedDate: body.shippedDate,
        note: body.note,
        cost: body.cost,
    }


    orderModel.findByIdAndUpdate(orderId, OrderUpdate, (error, data) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return res.status(200).json({
                status: "Success: Update order success",
                data: data
            })
        }
    })
}

const deleteOrderById = (req, res) => {
    //B1: Chuẩn bị dữ liệu
    let customerId = req.params.customerId;
    let orderId = req.params.orderId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "customer ID is not valid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad req",
            message: "Order ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            // Sau khi xóa xong 1 order khỏi collection cần xóa thêm orderID trong customer đang chứa nó
            customerModel.findByIdAndUpdate(customerId,
                {
                    $pull: { orders: orderId }
                },
                (err, updatedcustomer) => {
                    if (err) {
                        return res.status(500).json({
                            status: "Error 500: Internal server error",
                            message: err.message
                        })
                    } else {
                        return res.status(204).json({
                            status: "Success: Delete order success"
                        })
                    }
                })
        }
    })
}
// Export customer controller thành 1 module
module.exports = {
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById,
}
