const mongoose = require('mongoose');

const productModel = require("../modules/ProductModel");

const createProduct = (req, res) => {
    let body = req.body;
    if (!body.name) {
        return res.status(400).json({
            message: `name is required`
        })
    }
    if (!body.color) {
        return res.status(400).json({
            message: `color is required`
        })
    }
    if (!body.imageChild) {
        return res.status(400).json({
            message: `imageChild is required`
        })
    }
    if (!body.categories) {
        return res.status(400).json({
            message: `categories is required`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            message: `Type is required`
        })
    }
    if (!body.imageUrl) {
        return res.status(400).json({
            message: `imageUrl is required`
        })
    }
    if (!Number.isInteger(body.buyPrice) || body.buyPrice < 0) {
        return res.status(400).json({
            message: `buyPrice is invalid`
        })
    }
    let newProduct = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
        color: body.color,
        categories: body.categories,
        imageChild: body.imageChild
    }
    productModel.create(newProduct, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(201).json({
            message: `Create successful`,
            newProduct: data
        })
    })
}

//get all product
async function getAllProduct(req, res) {
    try {
        const limit = parseInt(req.query.limit) || 12;
        const page = parseInt(req.query.page) - 1 || 0;

        let filterName = req.query.filterName
        let filterMinPrice = req.query.filterMinPrice;
        let filterMaxPrice = req.query.filterMaxPrice;
        let filterColor = req.query.filterColor;
        let filterCategories = req.query.filterCategories;

        const regex = new RegExp(filterName, 'i');
        let condition = {};

        if (filterName) {
            condition.name = { $regex: regex };
        }
        if (filterMinPrice) {
            condition.buyPrice = { ...condition.buyPrice, $gte: filterMinPrice };
        }
        if (filterMaxPrice) {
            condition.buyPrice = { ...condition.buyPrice, $lte: filterMaxPrice };
        }
        // color
        if (filterColor !== "" && !Array.isArray(filterColor) && filterColor !== undefined) {
            filterColor = [filterColor]
        }
        if (Array.isArray(filterColor)) {
            condition.color = {
                $in: filterColor
            }
        }
        //categories
        if (filterCategories !== "" && !Array.isArray(filterCategories) && filterCategories !== undefined) {
            filterCategories = [filterCategories]
        }
        if (Array.isArray(filterCategories)) {
            condition.categories = {
                $in: filterCategories
            }
        }
        const product = await productModel.find(condition)
            .skip(page * limit)
            .limit(limit)
            .populate("type")
        const total = await productModel.countDocuments(condition)
        const response = {
            message: "get all products successful",
            total,
            page: page + 1,
            limit,
            product
        }
        res.status(200).json(response)
    } catch (error) {
        res.status(500).json(error)
    }
}

async function checkPagination(req, res) {
    try {
        const limit = parseInt(req.query.limit) || 4;
        const page = parseInt(req.query.page) - 1 || 0;
        let filterName = req.query.filterName
        const regex = new RegExp(filterName, 'i');
        let condition = {};
        if (filterName) {
            condition.name = { $regex: regex };
        }
        const product = await productModel.find(condition)
            .skip(page * limit)
            .limit(limit)
            .populate("type")
        const total = await productModel.countDocuments(condition)
        const response = {
            message: "get all products successful",
            total,
            page: page + 1,
            limit,
            product
        }
        res.status(200).json(response)
    } catch (error) {
        res.status(500).json(error)
    }
}

//get product by Id
const getProductById = (req, res) => {
    let productId = req.params.productId;
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: `productId is invalid`
        })
    }
    productModel.findById(productId)
        .populate('type')
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: error.message
                })
            }
            return res.status(200).json({
                message: ` get product by Id success`,
                product: data
            })
        })
}

//update product by Id
const updateProductById = (req, res) => {
    let productId = req.params.productId;
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: `productId is invalid`
        })
    }
    if (body.name !== undefined && body.name == "") {
        return res.status(400).json({
            message: `name is required`
        })
    }
    if (body.categories !== undefined && body.categories == "") {
        return res.status(400).json({
            message: `categories is required`
        })
    }
    if (body.color !== undefined && body.color == "") {
        return res.status(400).json({
            message: `color is required`
        })
    }
    if (body.imageChild !== undefined && body.imageChild == "") {
        return res.status(400).json({
            message: `imageChild is required`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(body.type)) {
        return res.status(400).json({
            message: `type is invalid`
        })
    }
    if (body.imageUrl !== undefined && body.imageUrl == "") {
        return res.status(400).json({
            message: `imageUrl is required`
        })
    }
    if (body.buyPrice !== undefined && (!Number.parseInt(body.buyPrice) || body.buyPrice < 0)) {
        return res.status(400).json({
            message: `buyPrice is invalid`
        })
    }
    let updateproduct = {
        name: body.name,
        type: body.type,
        imageUrl: body.imageUrl,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
        color: body.color,
        categories: body.categories,
        imageChild: body.imageChild
    }
    productModel.findByIdAndUpdate(productId, updateproduct, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(200).json({
            message: `Update successful`,
            updateProduct: data
        })
    })
}

//delete by product Id
const deleteByProductId = (req, res) => {
    let productId = req.params.productId;
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: `productId is invalid`
        })
    }
    productModel.findByIdAndDelete(productId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: error.message
            })
        }
        return res.status(204).json({
            message: `delete success`
        })
    })
}

module.exports = { createProduct, getAllProduct, getProductById, updateProductById, deleteByProductId, checkPagination }